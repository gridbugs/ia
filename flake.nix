{
  description = "Flake for building Infra Arcana";

  inputs.nixpkgs.url = "nixpkgs/nixos-21.11";

  outputs = { self, nixpkgs }:
    let
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    in
    {
      defaultPackage = forAllSystems (system:
      with import nixpkgs { system = system; };
      stdenv.mkDerivation {
        name = "ia";
        src = self;
        enableParallelBuilding = true;
        nativeBuildInputs = [ cmake ];
        buildInputs = [ SDL2 SDL2_image SDL2_mixer ]
          ++ lib.optionals stdenv.isDarwin [ darwin.apple_sdk.frameworks.Cocoa ];
        postInstall = ''
          mkdir -p $out/bin
          cp -rv target/ia/* $out/bin
          mv -v $out/bin/ia{,-bin}
          echo -e '#!/usr/bin/env bash\ncd "$( dirname "''${BASH_SOURCE[0]}" )" && ./ia-bin' > $out/bin/ia
          chmod a+x $out/bin/ia
        '';
      });
    };
}
